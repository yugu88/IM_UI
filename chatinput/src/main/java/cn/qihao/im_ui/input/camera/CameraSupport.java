package cn.qihao.im_ui.input.camera;


import cn.qihao.im_ui.input.listener.CameraEventListener;
import cn.qihao.im_ui.input.listener.OnCameraCallbackListener;


public interface CameraSupport {
    CameraSupport open(int cameraId, int width, int height, boolean isFacingBack);
    int getOrientation(int cameraId);
    void release();
    void takePicture();
    void setCameraCallbackListener(OnCameraCallbackListener listener);
    void setCameraEventListener(CameraEventListener listener);
    void startRecordingVideo();
    void cancelRecordingVideo();
    String finishRecordingVideo();
}
