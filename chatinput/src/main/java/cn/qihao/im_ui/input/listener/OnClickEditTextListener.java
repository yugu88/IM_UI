package cn.qihao.im_ui.input.listener;

/**
 * Created by caiyaoguan on 2017/6/8.
 */

public interface OnClickEditTextListener {
    void onTouchEditText();
}
